require = __meteor_bootstrap__.require
xml2js = require 'xml2js'
parser = new xml2js.Parser

Meteor.Router.add
  '/posts/:s': (s) ->
    Commands.insert
      raw: s

    if s is "EPG"
      fetchOnAirEpg()
      [200, s]
    else
      console.log s
      [200, s]

  '/clear': () ->
    Commands.remove {}

    [200, 'OK']

generateUrl = ->
  d = new Date()
  month = d.getMonth() + 1
  if month < 10 then month = "0" + month
  day = d.getDate()
  if day < 10 then day = "0" + day
  sdate = "" + d.getFullYear() + month + day
  shour = d.getHours()
  if shour < 10 then shour = "0" + shour
  url = "http://rey.rash.jp/junk/tv_rss.cgi?area=014&sdate=" + sdate + "&shour=" + shour + "&lhour=1"

fetchOnAirEpg = ->
  url = generateUrl()
  Meteor.http.get url, (err, result) ->
    if err then console.log err
    else if result.statusCode is 200
      xmlData = result.content
      #console.log xmlData
      parser.parseString xmlData, (err, result) ->
        #console.dir result
        itemRoot = result["rdf:RDF"].item
        #console.dir itemRoot[0]
        flushMessage itemRoot

flushMessage = (itemRoot) ->
  Commands.remove {}
  for item in itemRoot
    Commands.insert
      raw: item.title
